﻿using Jeikel.BL.DL;
using Jeikel.BL.Services.Interfaces;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI
{
    static class Program
    {
        static  Container container;

        public static void InitContainer()
        {
            container =  Registry.registerContainer();
            container.Verify();
        }
        public static T GetIntance<T>()
        {
            return (T)container.GetInstance(typeof(T));
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            InitContainer();

            //var handler = container.GetInstance<ITestInterfaceService>();

            //Console.WriteLine(handler.ReturnHelloWork());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(container.GetInstance<LoginView>());
        }
    }
}
