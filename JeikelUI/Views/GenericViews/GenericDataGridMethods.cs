﻿using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.GenericViews
{
    public class GenericDataGrid<T>: Form
    {
        private readonly DataGridView _currentTable;
        private List<T> _listDataSource { get; set; }
        private BindingList<T> _bindingList { get; set; }
        private T _currentEntity { get; set; }

        public BindingSource Bs { get; set; } = new BindingSource();



        public T currentEntity { get {
                return this._currentEntity;
            }
        }

        public bool EntityDeleted { get; private set; }

        private readonly IGenericService<T> _service;
        public GenericDataGrid(DataGridView currentTable,IGenericService<T> service, DefaultGridConfig defaultGridConfig = null)
        {
            var defaulSettings = defaultGridConfig ?? new DefaultGridConfig();
            _currentTable = currentTable;
            _service = service;


            currentTable.AutoGenerateColumns = defaulSettings.AutoGenerateColumns;
            currentTable.AllowUserToAddRows = defaulSettings.AllowUserToAddRows;
            currentTable.AllowUserToResizeRows = true;
            currentTable.AllowUserToResizeColumns = true;
            currentTable.AllowUserToAddRows = false;
            currentTable.AllowUserToDeleteRows = false;
            currentTable.MultiSelect = false;
            //cur

            _listDataSource = service.Repository.GetAll().ToList();

            this.RegisterEvents();
        }

        public GenericDataGrid(DataGridView currentTable, List<T> source, DefaultGridConfig defaultGridConfig = null)
        {
            var defaulSettings = defaultGridConfig ?? new DefaultGridConfig();
            _currentTable = currentTable;
            //_service = service;


            currentTable.AutoGenerateColumns = defaulSettings.AutoGenerateColumns;
            currentTable.AllowUserToAddRows = defaulSettings.AllowUserToAddRows;
            currentTable.AllowUserToResizeRows = true;
            currentTable.AllowUserToResizeColumns = true;
            currentTable.AllowUserToAddRows = false;
            currentTable.AllowUserToDeleteRows = false;
            currentTable.MultiSelect = false;
            //cur

            _listDataSource = source;

            

           // _currentTable.DataSource = Bs;

            this.RegisterEvents();
        }


        public void BuildTable(params GridColumn[] columnsInformation)
        {

            var columns = GenerateColumns(columnsInformation);


            _currentTable.Columns.AddRange(columns);

            _bindingList = new BindingList<T>(_listDataSource);

            _currentTable.DataSource = _bindingList;

            AdjusTableSize();

            //for (int i = 0; i < _currentTable.Columns.Count - 1; i++)
            //{
            //    _currentTable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //}
            //_currentTable.Columns[_currentTable.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //for (int i = 0; i < _currentTable.Columns.Count; i++)
            //{
            //    int colw = _currentTable.Columns[i].Width;
            //    _currentTable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //    _currentTable.Columns[i].Width = colw;
            //}
        }

        private DataGridViewColumn[] GenerateColumns(params GridColumn[] columnsInformation)
        {
            var columns = new List<DataGridViewColumn>();


              
            foreach (var column in columnsInformation)
            {
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn()
                {
                    CellTemplate = cell,
                    Name = column.ColumnName,
                    HeaderText = column.ColumnName,
                    DataPropertyName = column.ColumnDataName,
                    Resizable = DataGridViewTriState.True,
                    FillWeight= 100,
                    ReadOnly = true
                    //AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                };
                columns.Add(col);
            }

            return columns.ToArray();
        }

        private void AdjusTableSize()
        {
            _currentTable.AutoResizeColumns();

            _currentTable.AutoSizeColumnsMode =
            DataGridViewAutoSizeColumnsMode.Fill;

            //_currentTable.Width =
            //    _currentTable.Columns.Cast<DataGridViewColumn>().Sum(x => x.Width)
            //    + (_currentTable.RowHeadersVisible ? _currentTable.RowHeadersWidth : 0) + 8;

            //_currentTable.Height = 160;
        }

        


        public void RefreshTable()
        {
            if (_service  != null)
            {
                _listDataSource = _service.Repository.GetAll().ToList();
            }
            //_currentTable.DataSource = Bs;
            _currentTable.DataSource = null;
            _currentTable.DataSource = _listDataSource;
            _currentTable.Refresh();
        }

        public void DeletedElementFromTable()
        {
            this.EntityDeleted = true;
        }

        public void RegisterEvents()
        {

            _currentTable.SelectionChanged += getCurrentEntity;
            _currentTable.RowsAdded += getCurrentEntity;
            _currentTable.RowsRemoved += getCurrentEntity;
            _currentTable.ClearSelection();
        }



        #region events
        private void getCurrentEntity(object sender, EventArgs e)
        {
            if (this.EntityDeleted)
            {
                this.EntityDeleted = false;
                return;
            }
                 

                this._currentEntity = (T)_currentTable.CurrentRow?.DataBoundItem;
            ;
        }
        #endregion

    }

    public class GridColumn
    {
        public string ColumnName { get; set; }
        public string ColumnDataName { get; set; }
    }

    public class DefaultGridConfig
    {
        public bool AutoGenerateColumns { get; set; } = false;
        public bool AllowUserToAddRows { get; set; } = false;
    }
}
