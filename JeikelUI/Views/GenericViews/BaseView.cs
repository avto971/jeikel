﻿using System;
using System.Windows.Forms;

namespace JeikelUI.Views.GenericViews
{
    public class BaseView<T>: Form
    {

        public GenericDataGrid<T> Grid { get; set; }
        public T CurrentEntity { get { return this.Grid.currentEntity; } }

        

        private string AreaName { get; set; }
        private string TextName { get; set; }
        private int FormWidth { get; set; }
        private int FormHeight { get; set; }
        public bool IsEditing { get; set; }
        public BaseView()
        {
            //this.Size = new System.Drawing.Size(800, 800);
        }
        public BaseView(string areaName, string textName)
        {
            this.AreaName = areaName;
            this.TextName = textName;

        }
        public BaseView(string areaName, string textName, int formWidth, int formHeigth)
        {
            this.AreaName = areaName;
            this.TextName = textName;
            this.FormWidth = formWidth;
            this.FormHeight = formHeigth;

        }

        public virtual  void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Base Form
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 20F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = this.AreaName ?? "Form1";
            this.Text = this.TextName ?? "Form1";
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            //this.MaximizeBox = false;
            //this.MinimizeBox = false;

            this.ClientSize = new System.Drawing.Size(this.FormWidth, this.FormHeight);
            this.Size = new System.Drawing.Size(this.FormWidth, this.FormHeight);
            this.ResumeLayout(false);

        }

        public void ShowErrorModal(string message)
        {
            string caption = "Error";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Error);
        }

        public void ShowSavedModal()
        {
            string caption = "Información";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show("Registro agregado correctamente", caption, buttons, MessageBoxIcon.Information);
        }

        public void ConfirmDelete(Action callBack)
        {
            string caption = "Alerta!";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show("Está seguro que desea borrar el registro?", caption, buttons, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                callBack();
            }

        }

        public void ShowDeletedModal()
        {
           
            string caption = "Información";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show("Registro Borrado correctamente", caption, buttons, MessageBoxIcon.Information);
        }
    }
}
