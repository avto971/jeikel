﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.Core.Generic;
using JeikelUI.Views.Admin;
using JeikelUI.Views.GenericViews;
using JeikelUI.Views.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI
{
    public partial class LoginView : BaseView<object>
    {
        public LoginView() :base("Inicio de sesión",
            "Inicio de sesión", FormContstants.LOGINVIEWWIDTH, FormContstants.LOGINVIEWHEIGHT)
        {
            InitializeComponent();
            base.InitializeComponent();
            Initalize();

        }

        private void Initalize()
        {
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.MaxLength = 14;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var adminView = new AdminBaseView();

            adminView.Show();
            Hide();
          //  var service = Program.GetIntance<ITestInterfaceService>();
          //  service.AddTest();
          //Console.WriteLine(service.ReturnHelloWork());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var userBaseView = new UserBaseView();

            userBaseView.Show();
            Hide();
        }
    }
}
