﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.Admin
{
    public partial class LanguageMaintenance : BaseView<Language>
    {
        //public GenericDataGrid<Language> Grid { get; set; }
        //public Language CurrentEntity { get { return this.Grid.currentEntity; } }

        ILanguageService service = Program.GetIntance<ILanguageService>();
        public LanguageMaintenance() : base("Admin - Mantenimiento de Lenguaje",
            "Admin",FormContstants.ADMINMAINTENANCEWIDTH,FormContstants.ADMINMAINTENANCEHEIGHT)
        {
            InitializeComponent();
            base.InitializeComponent();
            fillTable();
        }
        public void fillTable()
        {
           
            Grid = new GenericDataGrid<Language>(dataGridView1, service);
            Grid.BuildTable(
                new GridColumn
                {
                    ColumnDataName = "Id",
                    ColumnName = "Id"
                },
                new GridColumn
                {
                    ColumnDataName = "LanguageName",
                    ColumnName = "Nombre Lenguaje"
                },
                new GridColumn
                {
                    ColumnDataName = "LanguageCode",
                    ColumnName = "Código"
                }
            );

        }

        private void button1_Click(object sender, EventArgs e)
        {




            var objToValidate = this.IsEditing ? CurrentEntity  : new Language
            {
                LanguageName = textBox1.Text,
                LanguageCode = textBox2.Text
            };

            if (CurrentEntity != null && this.IsEditing)
            {
                CurrentEntity.LanguageCode = textBox2.Text;
                CurrentEntity.LanguageName = textBox1.Text;
            }


            var actionResult = service.SaveOrEdit<LanguageValidator>(objToValidate, this.IsEditing);

            if (!actionResult.Success)
                base.ShowErrorModal(actionResult.Message);
            else
            {
                base.ShowSavedModal();
                Grid.RefreshTable();
                this.IsEditing = false;
                this.cleanFields();
            }
                



        }

        private void button4_Click(object sender, EventArgs e)
        {
            base.ConfirmDelete(() => {
                if (CurrentEntity != null)
                {
                    service.DeleteEntity(CurrentEntity, ()=> {
                        base.ShowDeletedModal();
                        this.cleanFields();
                        Grid.RefreshTable();
                    });
                }

            });
        }

        private void cleanFields()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CurrentEntity != null)
            {
                this.textBox1.Text = CurrentEntity.LanguageName;
                this.textBox2.Text = CurrentEntity.LanguageCode;
                this.IsEditing = true;
            }

        }
        private void button5_Click(object sender, EventArgs e)
        {
            this.cleanFields();
        }
    }
}
