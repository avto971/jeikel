﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.Admin
{
    public partial class AdminBaseView : Form
    {
        public AdminBaseView()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var languageMaint = new LanguageMaintenance();
            languageMaint.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var institutionMaintenance = new InstitutionMaintenance();
            institutionMaintenance.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var identificationTypeMaintenance = new IdentificationTypeMaintenance();
            identificationTypeMaintenance.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var departmentMaintenance = new DepartmentMaintenance();
            departmentMaintenance.Show();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            var competitionMaintenance = new CompetitionMaintenance();
            competitionMaintenance.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var jobPositionMaintenance = new JobPositionMaintenance();
            jobPositionMaintenance.Show();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            var requestCandidates = new RequestCandidates();
            requestCandidates.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var reporte = new EmployeeReportView();
            reporte.Show();
        }
    }
}
