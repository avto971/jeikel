﻿using Jeikel.BL.Services;
using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.Core.Enums;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;
using System;

namespace JeikelUI.Views.Admin
{
    public partial class JobPositionMaintenance :  BaseView<JobPosition>
    {
        IJobPositionService service = Program.GetIntance<IJobPositionService>();

        public JobPositionMaintenance() : base("Admin - Mantenimiento de Posiciones de trabajo",
            "Admin", FormContstants.ADMINMAINTENANCEWIDTH, FormContstants.ADMINMAINTENANCEHEIGHT)
        {
            InitializeComponent();
            base.InitializeComponent();
            fillTable();
            fillCombobox();
        }

        private void fillCombobox()
        {
            var values = EnumService.GetRiskLevel();
            comboBox1.DataSource = values;
            this.comboBox1.DisplayMember = "Key";
            this.comboBox1.ValueMember = "Value";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void fillTable()
        {

            Grid = new GenericDataGrid<JobPosition>(dataGridView1, service);
            Grid.BuildTable(
                new GridColumn
                {
                    ColumnDataName = "Id",
                    ColumnName = "Id"
                },
                new GridColumn
                {
                    ColumnDataName = "Name",
                    ColumnName = "Nombre"
                },
                new GridColumn
                {
                    ColumnDataName = "RiskLevel",
                    ColumnName = "Riesgo"
                },
                new GridColumn
                {
                    ColumnDataName = "MinimunSalary",
                    ColumnName = "Salario Mínimo"
                },
                new GridColumn
                {
                    ColumnDataName = "MaximumSalary",
                    ColumnName = "Salario máximo"
                }
            );

        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.cleanFields();

        }

        private void cleanFields()
        {
            this.textBox1.Text = "";
            this.comboBox1.SelectedValue = 0;
            this.textBox2.Text = "";
            this.textBox3.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            base.ConfirmDelete(() => {
                if (CurrentEntity != null)
                {
                    service.DeleteEntity(CurrentEntity, () => {
                        base.ShowDeletedModal();
                        this.cleanFields();
                        Grid.RefreshTable();
                    });
                }

            });
        }

        public void MapCurrentEntity()
        {
            CurrentEntity.Name = this.textBox1.Text;
            CurrentEntity.RiskLevel = (RiskLevel)comboBox1.SelectedValue;
            CurrentEntity.MinimunSalary = decimal.Parse(textBox2.Text);
            CurrentEntity.MaximumSalary = decimal.Parse(textBox3.Text);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CurrentEntity != null)
            {
                this.textBox1.Text = CurrentEntity.Name;
                comboBox1.SelectedIndex = (int)CurrentEntity.RiskLevel;
                textBox2.Text =  CurrentEntity.MinimunSalary.ToString();
                textBox3.Text = CurrentEntity.MaximumSalary.ToString();
                this.IsEditing = true;
                this.comboBox1.Refresh();
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {

            var objToValidate = this.IsEditing ? CurrentEntity : new JobPosition
            {
                Name = textBox1.Text,
                RiskLevel = (RiskLevel)comboBox1.SelectedValue,
                MinimunSalary = decimal.Parse(textBox2.Text),
                MaximumSalary = decimal.Parse(textBox3.Text)
        };

            if (CurrentEntity != null && this.IsEditing)
            {
                this.MapCurrentEntity();
            }


            var actionResult = service.SaveOrEdit<JobPositionValidator>(objToValidate, this.IsEditing);

            if (!actionResult.Success)
                base.ShowErrorModal(actionResult.Message);
            else
            {
                base.ShowSavedModal();
                Grid.RefreshTable();
                this.IsEditing = false;
                this.cleanFields();
            }
        }
    }
}
