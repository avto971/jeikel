﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jeikel.BL.Services.Interfaces;
using JeikelUI.Reports;
using Microsoft.Reporting.WinForms;
using static JeikelUI.Reports.Employee;

namespace JeikelUI.Views.Admin
{
    public partial class EmployeeReportView : Form
    {
        IEmployeeService employeeService = Program.GetIntance<IEmployeeService>();
        public EmployeeReportView()
        {
            InitializeComponent();
        }

        private void EmployeeReportView_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        internal void LoadReportData(List<EmployeeReportViewDs> listaEmpleados)
        {

            //var list = new List<EmployeeReportViewDs>
            //{
            //    new EmployeeReportViewDs
            //    {
            //        PrimerNombre = "wd",
            //        SegundoNombre = "yu",
            //        Tidentificacion = "cedula",
            //        NumeroIdentificacion = "65423",
            //        PosicionDeTrabajo = "sn",
            //        Departamento = "sdxs",
            //        FechaDeEntrada = "12 de enero 2018",
            //        Salario = "513"
            //    }
            //};

            //ds 

            if (this.reportViewer1.LocalReport.DataSources.Count <1)
            {
                var ds = new ReportDataSource("EmployeeDS");
                //Pasar por parametro la lista de los empleados
                ds.Value = listaEmpleados;

                this.reportViewer1.LocalReport.DataSources.Add(ds);

            }
            else
            {
                var ds = new ReportDataSource("EmployeeDS");
                //Pasar por parametro la lista de los empleados
                ds.Value = listaEmpleados;
                this.reportViewer1.LocalReport.DataSources.RemoveAt(0);

                this.reportViewer1.LocalReport.DataSources.Add(ds);

            }
            this.reportViewer1.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var listaReporte = new List<EmployeeReportViewDs>();
            var desde = this.dtDesde.Value;
            var hasta = this.dtHasta.Value;

            var empleados = this.employeeService.Repository.Find(x => 
            x.DateOfAdmission >= desde 
            && x.DateOfAdmission <= hasta).ToList();

            foreach (var empleado in empleados)
            {
                listaReporte.Add(new EmployeeReportViewDs
                {
                    PrimerNombre = empleado.Candidate.FirstName,
                    SegundoNombre = empleado.Candidate.LastName,
                    Tidentificacion = empleado.Candidate.IdentificationTypeName,
                    NumeroIdentificacion = empleado.Candidate.Identification,
                    PosicionDeTrabajo = empleado.Candidate.JobPositionName,
                    Departamento = empleado.Candidate.DepartmentName,
                    Salario = empleado.Salary.ToString(),
                    FechaDeEntrada = empleado.DateOfAdmission.ToString()
                });
            }

            this.LoadReportData(listaReporte);

        }
    }
    internal class EmployeeReportViewDs
    {

        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string Tidentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string PosicionDeTrabajo { get; set; }
        public string Departamento { get; set; }
        public string Salario { get; set; }
        public string FechaDeEntrada { get; set; }

    }
}
