﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;
using JeikelUI.Views.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.Admin
{
    public partial class RequestCandidates : BaseView<Candidate>
    {
        ICandidateService service = Program.GetIntance<ICandidateService>();

        public RequestCandidates() : base("Admin - Revision de solicitudes",
            "Admin", FormContstants.ADMINMAINTENANCEWIDTH, FormContstants.ADMINMAINTENANCEHEIGHT)
        {
            InitializeComponent();
            base.InitializeComponent();
            fillTable();


        }
        public void fillTable()
        {
            //var entities = service.Repository.GetAll(x => x.JobPosition).ToList();
            Grid = new GenericDataGrid<Candidate>(dataGridView1, service);

            Grid.BuildTable(
                new GridColumn
                {
                    ColumnDataName = "Id",
                    ColumnName = "Id"
                },
                new GridColumn
                {
                    ColumnDataName = "FirstName",
                    ColumnName = "Nombres"
                },
                new GridColumn
                {
                    ColumnDataName = "LastName",
                    ColumnName = "Apellidos"
                },
                new GridColumn
                {
                    ColumnDataName = "JobPositionName",
                    ColumnName = "Puesto al que aspira"
                }
            );

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var candidateView = new CandidateView(CurrentEntity, service);
            candidateView.Show();
        }
    }
}
