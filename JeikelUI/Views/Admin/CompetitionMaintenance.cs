﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;

namespace JeikelUI.Views.Admin
{
    public partial class CompetitionMaintenance : BaseView<Competences>
    {
        ICompetitionService service = Program.GetIntance<ICompetitionService>();

        public CompetitionMaintenance() : base("Admin - Mantenimiento de Competencias",
            "Admin", FormContstants.ADMINMAINTENANCEWIDTH, FormContstants.ADMINMAINTENANCEHEIGHT)
        {
            InitializeComponent();
            base.InitializeComponent();
            fillTable();
        }

        public void fillTable()
        {

            Grid = new GenericDataGrid<Competences>(dataGridView1, service);
            Grid.BuildTable(
                new GridColumn
                {
                    ColumnDataName = "Id",
                    ColumnName = "Id"
                },
                new GridColumn
                {
                    ColumnDataName = "Name",
                    ColumnName = "Nombre"
                },
                new GridColumn
                {
                    ColumnDataName = "Description",
                    ColumnName = "Descripción"
                }
            );

        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.cleanFields();

        }
        private void cleanFields()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            base.ConfirmDelete(() => {
                if (CurrentEntity != null)
                {
                    service.DeleteEntity(CurrentEntity, () => {
                        base.ShowDeletedModal();
                        this.cleanFields();
                        Grid.RefreshTable();
                    });
                }

            });
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            if (CurrentEntity != null)
            {
                this.textBox1.Text = CurrentEntity.Name;
                this.textBox2.Text = CurrentEntity.Description;
                this.IsEditing = true;
            }
        }

        private void button4_Click(object sender, System.EventArgs e)
        {

            var objToValidate = this.IsEditing ? CurrentEntity : new Competences
            {
                Name = textBox1.Text,
                Description = textBox2.Text
            };

            if (CurrentEntity != null && this.IsEditing)
            {
                CurrentEntity.Name = textBox1.Text;
                CurrentEntity.Description = textBox2.Text;
            }


            var actionResult = service.SaveOrEdit<CompetitionValidator>(objToValidate, this.IsEditing);

            if (!actionResult.Success)
                base.ShowErrorModal(actionResult.Message);
            else
            {
                base.ShowSavedModal();
                Grid.RefreshTable();
                this.IsEditing = false;
                this.cleanFields();
            }
        }
    }
}
