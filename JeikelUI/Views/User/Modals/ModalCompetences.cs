﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User.Modals
{
    public partial class ModalCompetences : Form
    {
        public List<Competences> Competences { get; set; }

        public List<CandidateCompetence> CandidateCompetences { get; set; }

        public ModalCompetences(List<CandidateCompetence> candidateCompetences)
        {
            InitializeComponent();
            this.ConfigureComponents();
            CandidateCompetences = candidateCompetences;
            
        }

        public void ConfigureComponents()
        {
            

            this.cbCompetencias.DisplayMember = "Name";
            this.cbCompetencias.ValueMember = "Id";
            this.LoadCompetences();
        }

        public void LoadCompetences()
        {
            var competitionService = Program.GetIntance<ICompetitionService>();

            Competences = competitionService.Repository.GetAll().ToList();

            this.cbCompetencias.DataSource = Competences;
        }

        public void OnAddCompetence(Action<Competences> callBack)
        {

        }

        private void btnAgregarCompetencia_Click(object sender, EventArgs e)
        {
            var comboBoxValue = (Competences)this.cbCompetencias.SelectedItem;
            var candidateCompetence = new CandidateCompetence
            {
                CompetitionId = comboBoxValue.Id,
                CompetitionName = comboBoxValue.Name
            };
            CandidateCompetences.Add(candidateCompetence);
        }
    }
}
