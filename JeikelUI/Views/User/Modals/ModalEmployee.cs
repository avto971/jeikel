﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User.Modals
{
    public partial class ModalEmployee : Form
    {
        public IEmployeeService service = Program.GetIntance<IEmployeeService>();
        public readonly Candidate Candidate; 
        public ModalEmployee(Candidate candidate)
        {
            InitializeComponent();
            Candidate = candidate;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var employee = new Employee
            {
                CandidateId = Candidate.Id,
                Salary = decimal.Parse(textBox1.Text),
                DateOfAdmission = dateTimePicker1.Value,
            };
            service.Repository.ValidateEntity<EmployeeValidator>(employee, (entity) => {
                service.Repository.Add(entity);
                service.Repository.SaveChanges();
            });

            this.Close();
        }
    }
}
