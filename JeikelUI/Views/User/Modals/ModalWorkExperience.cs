﻿using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User.Modals
{
    public partial class ModalWorkExperience : Form
    {
        public List<CandidateWorkExperience> CandidateWorkExperiences { get; set; }

        public ModalWorkExperience(List<CandidateWorkExperience> candidateWorkExperiences)
        {
            InitializeComponent();
            CandidateWorkExperiences =  candidateWorkExperiences;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var candidateWorkExperience = new CandidateWorkExperience
            {
                Salary = decimal.Parse(this.txtSalario.Text),
                Company = this.txtCompania.Text,
                JobPositionOccuped = this.txtPosicionOcupada.Text,
                From = this.dtDesde.Value,
                Until = this.dtHasta.Value
            };

            CandidateWorkExperiences.Add(candidateWorkExperience);
        }
    }
}
