﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User.Modals
{
    public partial class ModalLanguages : Form
    {
        public List<Language> Institutions { get; set; }
        public List<CandidateLanguage> CandidateLanguages { get; set; }

        public ModalLanguages(List<CandidateLanguage> candidateLanguages)
        {
            InitializeComponent();
            this.ConfigureComponents();
            CandidateLanguages = candidateLanguages;
        }

        public void ConfigureComponents()
        {


            this.cbIdiomas.DisplayMember = "LanguageName";
            this.cbIdiomas.ValueMember = "Id";

            this.LoadComboBoxs();
        }


        public void LoadComboBoxs()
        {
            var institutionService = Program.GetIntance<ILanguageService>();

            Institutions = institutionService.Repository.GetAll().ToList();

            this.cbIdiomas.DataSource = Institutions;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var cbItem = (Language)cbIdiomas.SelectedItem;
            var languageCandidate = new CandidateLanguage
            {
                LanguageId = cbItem.Id,
                LanguageName = cbItem.LanguageName
            };
            CandidateLanguages.Add(languageCandidate);
        }
    }
}
