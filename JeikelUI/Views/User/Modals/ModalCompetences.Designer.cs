﻿namespace JeikelUI.Views.User.Modals
{
    partial class ModalCompetences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCompetencias = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAgregarCompetencia = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbCompetencias
            // 
            this.cbCompetencias.FormattingEnabled = true;
            this.cbCompetencias.Location = new System.Drawing.Point(114, 208);
            this.cbCompetencias.Name = "cbCompetencias";
            this.cbCompetencias.Size = new System.Drawing.Size(473, 39);
            this.cbCompetencias.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Competencia";
            // 
            // btnAgregarCompetencia
            // 
            this.btnAgregarCompetencia.Location = new System.Drawing.Point(363, 342);
            this.btnAgregarCompetencia.Name = "btnAgregarCompetencia";
            this.btnAgregarCompetencia.Size = new System.Drawing.Size(276, 82);
            this.btnAgregarCompetencia.TabIndex = 2;
            this.btnAgregarCompetencia.Text = "Agregar";
            this.btnAgregarCompetencia.UseVisualStyleBackColor = true;
            this.btnAgregarCompetencia.Click += new System.EventHandler(this.btnAgregarCompetencia_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15.9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(104, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(475, 54);
            this.label2.TabIndex = 3;
            this.label2.Text = "Agregar Competencia";
            // 
            // ModalCompetences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 499);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAgregarCompetencia);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCompetencias);
            this.Name = "ModalCompetences";
            this.Text = "ModalAddCompetences";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCompetencias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAgregarCompetencia;
        private System.Windows.Forms.Label label2;
    }
}