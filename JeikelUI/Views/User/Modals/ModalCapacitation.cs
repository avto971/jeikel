﻿using Jeikel.BL.Services;
using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.Core.Enums;
using Jeikel.Core.Extensions;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User.Modals
{
    public partial class ModalCapacitation : Form
    {
        public List<Institution> Institutions { get; set; }
        public List<CandidateCapacitation> CandidateCapacitations { get; set; }
        private IGenericRepository<CandidateCapacitation> genericRepository { get; set; } = Program.GetIntance<IGenericRepository<CandidateCapacitation>>();

        public ModalCapacitation(List<CandidateCapacitation> candidateCapacitations)
        {
            InitializeComponent();
            this.ConfigureComponents();
            CandidateCapacitations = candidateCapacitations;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        public void ConfigureComponents()
        {


            this.cbInstitucion.DisplayMember = "Name";
            this.cbInstitucion.ValueMember = "Id";

            this.cbNivelCapacitacion.DisplayMember = "Key";
            this.cbNivelCapacitacion.ValueMember = "Value";

            this.LoadComboBoxs();
        }

        public void LoadComboBoxs()
        {
            var institutionService = Program.GetIntance<IInstitutionService>();

            Institutions = institutionService.Repository.GetAll().ToList();

            this.cbInstitucion.DataSource = Institutions;

            this.cbNivelCapacitacion.DataSource = EnumService.GetCapacitationLevel().ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var institution = (Institution)cbInstitucion.SelectedItem;
            var nivelCapacitation = (SelectItemOption)cbNivelCapacitacion.SelectedItem;

            var candidateCapacitation = new CandidateCapacitation
            {
                InstitutionName = institution.Name,
                IntitutionId = institution?.Id ?? 0,
                Description = txtDescription.Text,
                CapacitationLevelName = nivelCapacitation.Key,
                Level = (CapacitationLevel)this.cbNivelCapacitacion?.SelectedValue,
                From = this.dtDesde.Value,
                Until = this.dtHasta.Value
            };
            var validate = genericRepository.ValidateEntity<CandidateCapacitationValidator>(candidateCapacitation, (candidate) =>
            {
                CandidateCapacitations.Add(candidateCapacitation);
                this.CleanFields();
            });
            if (!validate.Success)
            {
                string caption = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(validate.Message, caption, buttons, MessageBoxIcon.Error);
            }

        }
        private void CleanFields()
        {
            this.txtDescription.Text = "";
            this.cbNivelCapacitacion.SelectedValue = 1;
            this.cbInstitucion.SelectedValue = 1;
            this.dtDesde.Text = "";
            this.dtHasta.Text = "";

        }
    }
}
