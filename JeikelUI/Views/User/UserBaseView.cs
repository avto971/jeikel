﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;
using System;

namespace JeikelUI.Views.User
{
    public partial class UserBaseView :  BaseView<JobPosition>
    {
        IJobPositionService service = Program.GetIntance<IJobPositionService>();

        public UserBaseView() : base("Usuario", "Aspirar a puesto", 750, 390)
        {
            InitializeComponent();
            this.fillTable();
            base.InitializeComponent();
        }

        public void fillTable() 
        {

            Grid = new GenericDataGrid<JobPosition>(dataGridView1, service);
            Grid.BuildTable(
               new GridColumn
               {
                   ColumnDataName = "Id",
                   ColumnName = "Id"
               },
                new GridColumn
                {
                    ColumnDataName = "Name",
                    ColumnName = "Nombre"
                },
                new GridColumn
                {
                    ColumnDataName = "RiskLevel",
                    ColumnName = "Riesgo"
                },
                new GridColumn
                {
                    ColumnDataName = "MinimunSalary",
                    ColumnName = "Salario Mínimo"
                },
                new GridColumn
                {
                    ColumnDataName = "MaximumSalary",
                    ColumnName = "Salario máximo"
                }
            );

        }


        private void button1_Click(object sender, EventArgs e)
        {
            var candidateView = new CandidateView(CurrentEntity);
            candidateView.Show();
        }
    }
}
