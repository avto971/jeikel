﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.DataModel.Model;
using JeikelUI.Views.GenericViews;
using JeikelUI.Views.User.Modals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeikelUI.Views.User
{
    public partial class CandidateView : BaseView<Candidate>
    {
        public new Candidate CurrentEntity { get; set; } = new Candidate();

        public List<Department> Departments { get; set; }
        public List<IdentificationType> IdentificationTypes { get; set; }

        public GenericDataGrid<CandidateCompetence> CandidateCompetenceGrid { get; set; }
        public GenericDataGrid<CandidateCapacitation> CandidateCapacitacionesGrid { get; set; }
        public GenericDataGrid<CandidateWorkExperience> CandidateWorkExperienceGrid { get; set; }
        public GenericDataGrid<CandidateLanguage> CandidateLanguageGrid { get; set; }

        public JobPosition JobPositionToApply { get; set; }


        ICandidateService service = Program.GetIntance<ICandidateService>();

        public string CurrentTab { get; set; } = "tabCompetencias";
        public CandidateView()
        {
            InitializeComponent();
        }
        public CandidateView(JobPosition jobPositionToApply) :base("Usuario","Aspirar a puesto", 740 , 600)
        {
            InitializeComponent();
            this.lblNombrePuestoAspira.Text += jobPositionToApply.Name;
            base.InitializeComponent();
            this.RegisterEvents();
            this.ConfigureComponents();
            JobPositionToApply = jobPositionToApply;
        }
        public CandidateView(Candidate Candidate, ICandidateService _service) : base("Usuario", "Aspirar a puesto", 740, 600)
        {
            InitializeComponent();
            base.InitializeComponent();
            CurrentEntity = Candidate;
            this.ConfigureComponents();
            this.LoadComboBoxs();
            this.BuildAdminVerifyView();
            service = _service;
        }

        public void InitializeCandidateViewComponents()
        {
            
        }

        public void BuildAdminVerifyView()
        {
            // Activating and Disactivating Controls
            this.btnGuardar.Visible = false;
            this.btnRechazarSolicitud.Visible = false;
            this.btnAceptarSolicitud.Visible = false;
            if (CurrentEntity.CandidateRequestStatus == Jeikel.Core.Enums.CandidateRequestStatus.Espera)
            {
                this.btnRechazarSolicitud.Visible = true;
                this.btnAceptarSolicitud.Visible = true;
            }

            this.lblNombrePuestoAspira.Visible = false;
            this.btnAgregarInformacionComplementaria.Visible = false;
            this.btnEliminarInformacionComplementaria.Visible = false;
            //Filling Properties with the current value
            txtName.Text = CurrentEntity.FirstName;
            txtApellido.Text = CurrentEntity.LastName;
            cbTipoIdentificacion.SelectedValue = CurrentEntity.IdentificationTypeId;
            txtNumeroIdentifacion.Text = CurrentEntity.Identification;
            txtSalarioAspirado.Text = CurrentEntity.SalaryDesired.ToString();
            cbDepartamento.SelectedValue = CurrentEntity.DepartmentId;
            txtRecomendadoPor.Text = CurrentEntity.RecommendedBy;

            lblEstadoSolicitud.Text = "Estado de la solicitud: " + CurrentEntity.CandidateRequestStatus;
            lblEstadoSolicitud.Visible = true;

            //Disabling Controls
            txtName.ReadOnly = true;
            txtApellido.ReadOnly = true;
            cbTipoIdentificacion.Enabled = false;
            txtNumeroIdentifacion.ReadOnly = true;
            txtSalarioAspirado.ReadOnly = true;
            cbDepartamento.Enabled = false;
            txtRecomendadoPor.ReadOnly = true;

            //Filling Properties TO Show on table
            this.CurrentEntity.CandidatesCompetences.ForEach((competence) => {
                competence.CompetitionName = competence.Competition?.Name;
            });

            this.CurrentEntity.CandidateCapacitations.ForEach((capacitation) => {
                capacitation.InstitutionName = capacitation.Intitution?.Name;
                capacitation.CapacitationLevelName = capacitation.Level.ToString();
            });

            this.CurrentEntity.CandidateLanguages.ForEach((language) => {
                language.LanguageName = language.Language?.LanguageName;
            });
            //End Filling props
            //Refreshing Tables
            this.CandidateCompetenceGrid.Refresh();
            this.CandidateLanguageGrid.Refresh();
            this.CandidateCapacitacionesGrid.Refresh();
            this.CandidateWorkExperienceGrid.Refresh();
        }

        public void RegisterEvents()
        {
            this.tabControl1.Selected += TabChanged;

        }

  

        public void ConfigureComponents()
        {

            #region COmboboxs
                this.cbDepartamento.DisplayMember = "Name";
                this.cbDepartamento.ValueMember = "Id";

                this.cbTipoIdentificacion.DisplayMember = "Name";
                this.cbTipoIdentificacion.ValueMember = "Id";

                this.LoadComboBoxs();

            #endregion

            #region Tables

            #region TableCompeteces

            CandidateCompetenceGrid = new GenericDataGrid<CandidateCompetence>(this.tblCompetencias, CurrentEntity.CandidatesCompetences);
            CandidateCompetenceGrid.BuildTable(
                    
                new GridColumn
                {
                    ColumnDataName = "CompetitionId",
                    ColumnName = "Id"
                },
                new GridColumn
                {
                    ColumnDataName = "CompetitionName",
                    ColumnName = "Nombre Competencia"
                }
            );

        #endregion

            #region TableCapacitaciones
           


            CandidateCapacitacionesGrid = new GenericDataGrid<CandidateCapacitation>(this.tblCapacitaciones, CurrentEntity.CandidateCapacitations);
            CandidateCapacitacionesGrid.BuildTable(

                new GridColumn
                {
                    ColumnDataName = "InstitutionName",
                    ColumnName = "Nombre Institución"
                },
                new GridColumn
                {
                    ColumnDataName = "From",
                    ColumnName = "Desde"
                },
                new GridColumn
                {
                    ColumnDataName = "Until",
                    ColumnName = "Hasta"
                },
                new GridColumn
                {
                    ColumnDataName = "CapacitationLevelName",
                    ColumnName = "Nivel Obtenido"
                },
                new GridColumn
                {
                    ColumnDataName = "Description",
                    ColumnName = "Información"
                }
            );

            #endregion

            #region CandidateWorlExperience
            CandidateWorkExperienceGrid = new GenericDataGrid<CandidateWorkExperience>(this.tblExperienciaLaboral, CurrentEntity.CandidateWorkExperiences);
            CandidateWorkExperienceGrid.BuildTable(

                new GridColumn
                {
                    ColumnDataName = "Salary",
                    ColumnName = "Salario Devengado"
                },
                new GridColumn
                {
                    ColumnDataName = "Company",
                    ColumnName = "Compañía"
                },
                new GridColumn
                {
                    ColumnDataName = "JobPositionOccuped",
                    ColumnName = "Posición ocupada"
                },
                new GridColumn
                {
                    ColumnDataName = "From",
                    ColumnName = "Desde"
                }
                ,
                new GridColumn
                {
                    ColumnDataName = "Until",
                    ColumnName = "Hasta"
                }
            );

            #endregion

            #region TablaIdiomas
            CandidateLanguageGrid = new GenericDataGrid<CandidateLanguage>(this.tblIdiomas, CurrentEntity.CandidateLanguages);
            CandidateLanguageGrid.BuildTable(

                new GridColumn
                {
                    ColumnDataName = "LanguageName",
                    ColumnName = "Idioma"
                }
            );
            #endregion

            #endregion

        }


        public void LoadComboBoxs()
        {
            var departmentService = Program.GetIntance<IDepartmentService>();
            Departments = departmentService.Repository.GetAll().ToList();
            this.cbDepartamento.DataSource = Departments;

            var identificationTypeService = Program.GetIntance<IIdentificationTypeService>();
            IdentificationTypes = identificationTypeService.Repository.GetAll().ToList();
            this.cbTipoIdentificacion.DataSource = IdentificationTypes;
        }




        private void btnAgregarInformacionComplementaria_Click(object sender, EventArgs e)
        {
            switch (CurrentTab)
            {
                case "tabCompetencias":
                    var modalCompetencias = new ModalCompetences(CurrentEntity.CandidatesCompetences);
                    modalCompetencias.FormClosed += ModalCompetencias_FormClosed;
                    modalCompetencias.Show();
                    break;

                case "tabCapacitaciones":
                    var modalCapacitaciones = new ModalCapacitation(CurrentEntity.CandidateCapacitations);
                    modalCapacitaciones.FormClosed += ModalCapacitaciones_FormClosed;
                    modalCapacitaciones.Show();
                    break; 
                    
                case "tabExperienciaLaboral":
                    var modalExperienciaLaboral = new ModalWorkExperience(CurrentEntity.CandidateWorkExperiences);
                    modalExperienciaLaboral.FormClosed += ModalExperienciaLaboral_FormClosed;
                    modalExperienciaLaboral.Show();
                    break;
                case "tabIdiomas":
                    var modalIdiomas = new ModalLanguages(CurrentEntity.CandidateLanguages);
                    modalIdiomas.FormClosed += ModalIdiomas_FormClosed;
                    modalIdiomas.Show();
                    break;


            }
        }







        #region Events
        private void TabChanged(object sender, TabControlEventArgs e)
            {
                CurrentTab = e.TabPage.Name;
            }
            private void ModalCompetencias_FormClosed(object sender, FormClosedEventArgs e)
            {
                this.CandidateCompetenceGrid.RefreshTable();
            }
            private void ModalCapacitaciones_FormClosed(object sender, FormClosedEventArgs e)
            {
                this.CandidateCapacitacionesGrid.RefreshTable();
            }
            private void ModalExperienciaLaboral_FormClosed(object sender, FormClosedEventArgs e)
            {
                this.CandidateWorkExperienceGrid.RefreshTable();
                //this.tblExperienciaLaboral.Refresh();
            }
            private void ModalIdiomas_FormClosed(object sender, FormClosedEventArgs e)
            {
                this.CandidateLanguageGrid.RefreshTable();
                this.tblIdiomas.Refresh();
            }
            private void ModalEmployee_FormClosed(object sender, FormClosedEventArgs e)
            {
                CurrentEntity.CandidateRequestStatus = Jeikel.Core.Enums.CandidateRequestStatus.Aprobada;
                service.Repository.Update(CurrentEntity);

                service.Repository.SaveChanges();

                base.ShowSavedModal();

                BuildAdminVerifyView();

            }

        #endregion

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var department = (Department)this.cbDepartamento.SelectedItem;
            var IdentificationType = (IdentificationType)this.cbTipoIdentificacion.SelectedItem;

            CurrentEntity.FirstName = txtName.Text;
            CurrentEntity.LastName = txtApellido.Text;
            CurrentEntity.RecommendedBy = txtRecomendadoPor.Text;
            CurrentEntity.JobPositionId = JobPositionToApply.Id;
            CurrentEntity.IdentificationTypeId = IdentificationType.Id;
            CurrentEntity.DepartmentId = department.Id;
            CurrentEntity.Identification = txtNumeroIdentifacion.Text;
            CurrentEntity.SalaryDesired =  decimal.Parse(txtSalarioAspirado.Text);
            CurrentEntity.IdentificationTypeName = IdentificationType.Name;

            var actionResult = service.SaveOrEdit<CandidateValidator>(CurrentEntity, false);

            if (!actionResult.Success)
                base.ShowErrorModal(actionResult.Message);
            else
            {
                string caption = "Información";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show("Su solicitud ha sido enviada correctamente", caption, buttons, MessageBoxIcon.Information);

                if (result  == DialogResult.OK)
                {
                    var userBaseView = new UserBaseView();
                    userBaseView.Show();
                    this.Hide();
                }
            }



        }

        private void btnAceptarSolicitud_Click(object sender, EventArgs e)
        {
            var modalEmployee = new ModalEmployee(CurrentEntity);
            modalEmployee.FormClosed += ModalEmployee_FormClosed;
            modalEmployee.Show();
        }

        private void btnRechazarSolicitud_Click(object sender, EventArgs e)
        {
            string caption = "Alerta!";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show("Está seguro que desea Rechazar al candidato?", caption, buttons, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                CurrentEntity.CandidateRequestStatus = Jeikel.Core.Enums.CandidateRequestStatus.Rechazada;
                service.Repository.Update(CurrentEntity);
                service.Repository.SaveChanges();
                this.BuildAdminVerifyView();
            }
        }

        private void btnEliminarInformacionComplementaria_Click(object sender, EventArgs e)
        {
            switch (CurrentTab)
            {
                case "tabCompetencias":
                    var cure = CandidateCompetenceGrid.currentEntity;
                    CurrentEntity.CandidatesCompetences.Remove(cure);
                    CandidateCompetenceGrid.DeletedElementFromTable();
                    CandidateCompetenceGrid.RefreshTable();
                    break;

                case "tabCapacitaciones":
                    var capa = CandidateCapacitacionesGrid.currentEntity;
                    CurrentEntity.CandidateCapacitations.Remove(capa);
                    CandidateCapacitacionesGrid.DeletedElementFromTable();
                    CandidateCapacitacionesGrid.RefreshTable();
                    break;

                case "tabExperienciaLaboral":
                    var work = CandidateWorkExperienceGrid.currentEntity;
                    CurrentEntity.CandidateWorkExperiences.Remove(work);
                    CandidateWorkExperienceGrid.DeletedElementFromTable();
                    CandidateWorkExperienceGrid.RefreshTable();
                    break;
                case "tabIdiomas":
                    var idio = CandidateLanguageGrid.currentEntity;
                    CurrentEntity.CandidateLanguages.Remove(idio);
                    CandidateLanguageGrid.DeletedElementFromTable();
                    CandidateLanguageGrid.RefreshTable();
                    break;


            }
        }
    }
}
