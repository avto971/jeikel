﻿namespace JeikelUI.Views.User
{
    partial class CandidateView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbTipoIdentificacion = new System.Windows.Forms.ComboBox();
            this.txtNumeroIdentifacion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtRecomendadoPor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDepartamento = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSalarioAspirado = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnEliminarInformacionComplementaria = new System.Windows.Forms.Button();
            this.btnAgregarInformacionComplementaria = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCompetencias = new System.Windows.Forms.TabPage();
            this.tblCompetencias = new System.Windows.Forms.DataGridView();
            this.tabCapacitaciones = new System.Windows.Forms.TabPage();
            this.tblCapacitaciones = new System.Windows.Forms.DataGridView();
            this.tabExperienciaLaboral = new System.Windows.Forms.TabPage();
            this.tblExperienciaLaboral = new System.Windows.Forms.DataGridView();
            this.tabIdiomas = new System.Windows.Forms.TabPage();
            this.tblIdiomas = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lblNombrePuestoAspira = new System.Windows.Forms.Label();
            this.btnRechazarSolicitud = new System.Windows.Forms.Button();
            this.lblEstadoSolicitud = new System.Windows.Forms.Label();
            this.btnAceptarSolicitud = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabCompetencias.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCompetencias)).BeginInit();
            this.tabCapacitaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCapacitaciones)).BeginInit();
            this.tabExperienciaLaboral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblExperienciaLaboral)).BeginInit();
            this.tabIdiomas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblIdiomas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbTipoIdentificacion);
            this.groupBox1.Controls.Add(this.txtNumeroIdentifacion);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtApellido);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(39, 165);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(973, 508);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información Personal";
            // 
            // cbTipoIdentificacion
            // 
            this.cbTipoIdentificacion.FormattingEnabled = true;
            this.cbTipoIdentificacion.Location = new System.Drawing.Point(60, 335);
            this.cbTipoIdentificacion.Name = "cbTipoIdentificacion";
            this.cbTipoIdentificacion.Size = new System.Drawing.Size(348, 39);
            this.cbTipoIdentificacion.TabIndex = 7;
            // 
            // txtNumeroIdentifacion
            // 
            this.txtNumeroIdentifacion.Location = new System.Drawing.Point(544, 335);
            this.txtNumeroIdentifacion.Name = "txtNumeroIdentifacion";
            this.txtNumeroIdentifacion.Size = new System.Drawing.Size(349, 38);
            this.txtNumeroIdentifacion.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(538, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(276, 32);
            this.label4.TabIndex = 5;
            this.label4.Text = "Número Identifiación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 278);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(271, 32);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tipo de Identifiación";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(544, 160);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(349, 38);
            this.txtApellido.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(60, 160);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(348, 38);
            this.txtName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(538, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido(s)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre(s)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtRecomendadoPor);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cbDepartamento);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtSalarioAspirado);
            this.groupBox2.Location = new System.Drawing.Point(1163, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(664, 508);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Información del puesto";
            // 
            // txtRecomendadoPor
            // 
            this.txtRecomendadoPor.Location = new System.Drawing.Point(68, 440);
            this.txtRecomendadoPor.Name = "txtRecomendadoPor";
            this.txtRecomendadoPor.Size = new System.Drawing.Size(404, 38);
            this.txtRecomendadoPor.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(62, 394);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(251, 32);
            this.label7.TabIndex = 8;
            this.label7.Text = "Recomendado Por";
            // 
            // cbDepartamento
            // 
            this.cbDepartamento.FormattingEnabled = true;
            this.cbDepartamento.Location = new System.Drawing.Point(68, 305);
            this.cbDepartamento.Name = "cbDepartamento";
            this.cbDepartamento.Size = new System.Drawing.Size(404, 39);
            this.cbDepartamento.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(195, 32);
            this.label6.TabIndex = 11;
            this.label6.Text = "Departamento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(62, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(275, 32);
            this.label5.TabIndex = 10;
            this.label5.Text = "Salario al que aspira";
            // 
            // txtSalarioAspirado
            // 
            this.txtSalarioAspirado.Location = new System.Drawing.Point(68, 160);
            this.txtSalarioAspirado.Name = "txtSalarioAspirado";
            this.txtSalarioAspirado.Size = new System.Drawing.Size(404, 38);
            this.txtSalarioAspirado.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnEliminarInformacionComplementaria);
            this.groupBox3.Controls.Add(this.btnAgregarInformacionComplementaria);
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Location = new System.Drawing.Point(39, 702);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1788, 480);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Información complementaria";
            // 
            // btnEliminarInformacionComplementaria
            // 
            this.btnEliminarInformacionComplementaria.Location = new System.Drawing.Point(1680, 268);
            this.btnEliminarInformacionComplementaria.Name = "btnEliminarInformacionComplementaria";
            this.btnEliminarInformacionComplementaria.Size = new System.Drawing.Size(75, 62);
            this.btnEliminarInformacionComplementaria.TabIndex = 1;
            this.btnEliminarInformacionComplementaria.Text = "-";
            this.btnEliminarInformacionComplementaria.UseVisualStyleBackColor = true;
            this.btnEliminarInformacionComplementaria.Click += new System.EventHandler(this.btnEliminarInformacionComplementaria_Click);
            // 
            // btnAgregarInformacionComplementaria
            // 
            this.btnAgregarInformacionComplementaria.Location = new System.Drawing.Point(1680, 117);
            this.btnAgregarInformacionComplementaria.Name = "btnAgregarInformacionComplementaria";
            this.btnAgregarInformacionComplementaria.Size = new System.Drawing.Size(75, 62);
            this.btnAgregarInformacionComplementaria.TabIndex = 0;
            this.btnAgregarInformacionComplementaria.Text = "+";
            this.btnAgregarInformacionComplementaria.UseVisualStyleBackColor = true;
            this.btnAgregarInformacionComplementaria.Click += new System.EventHandler(this.btnAgregarInformacionComplementaria_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCompetencias);
            this.tabControl1.Controls.Add(this.tabCapacitaciones);
            this.tabControl1.Controls.Add(this.tabExperienciaLaboral);
            this.tabControl1.Controls.Add(this.tabIdiomas);
            this.tabControl1.Location = new System.Drawing.Point(23, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1633, 414);
            this.tabControl1.TabIndex = 0;
            // 
            // tabCompetencias
            // 
            this.tabCompetencias.Controls.Add(this.tblCompetencias);
            this.tabCompetencias.Location = new System.Drawing.Point(10, 48);
            this.tabCompetencias.Name = "tabCompetencias";
            this.tabCompetencias.Padding = new System.Windows.Forms.Padding(3);
            this.tabCompetencias.Size = new System.Drawing.Size(1613, 356);
            this.tabCompetencias.TabIndex = 0;
            this.tabCompetencias.Text = "Competencias";
            this.tabCompetencias.UseVisualStyleBackColor = true;
            // 
            // tblCompetencias
            // 
            this.tblCompetencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblCompetencias.Location = new System.Drawing.Point(30, 32);
            this.tblCompetencias.Name = "tblCompetencias";
            this.tblCompetencias.RowTemplate.Height = 40;
            this.tblCompetencias.Size = new System.Drawing.Size(1555, 293);
            this.tblCompetencias.TabIndex = 0;
            // 
            // tabCapacitaciones
            // 
            this.tabCapacitaciones.Controls.Add(this.tblCapacitaciones);
            this.tabCapacitaciones.Location = new System.Drawing.Point(10, 48);
            this.tabCapacitaciones.Name = "tabCapacitaciones";
            this.tabCapacitaciones.Padding = new System.Windows.Forms.Padding(3);
            this.tabCapacitaciones.Size = new System.Drawing.Size(1613, 356);
            this.tabCapacitaciones.TabIndex = 1;
            this.tabCapacitaciones.Text = "Capacitaciones";
            this.tabCapacitaciones.UseVisualStyleBackColor = true;
            // 
            // tblCapacitaciones
            // 
            this.tblCapacitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblCapacitaciones.Location = new System.Drawing.Point(30, 32);
            this.tblCapacitaciones.Name = "tblCapacitaciones";
            this.tblCapacitaciones.RowTemplate.Height = 40;
            this.tblCapacitaciones.Size = new System.Drawing.Size(1557, 297);
            this.tblCapacitaciones.TabIndex = 0;
            // 
            // tabExperienciaLaboral
            // 
            this.tabExperienciaLaboral.Controls.Add(this.tblExperienciaLaboral);
            this.tabExperienciaLaboral.Location = new System.Drawing.Point(10, 48);
            this.tabExperienciaLaboral.Name = "tabExperienciaLaboral";
            this.tabExperienciaLaboral.Size = new System.Drawing.Size(1613, 356);
            this.tabExperienciaLaboral.TabIndex = 2;
            this.tabExperienciaLaboral.Text = "Experiencia Laboral";
            this.tabExperienciaLaboral.UseVisualStyleBackColor = true;
            // 
            // tblExperienciaLaboral
            // 
            this.tblExperienciaLaboral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblExperienciaLaboral.Location = new System.Drawing.Point(28, 45);
            this.tblExperienciaLaboral.Name = "tblExperienciaLaboral";
            this.tblExperienciaLaboral.RowTemplate.Height = 40;
            this.tblExperienciaLaboral.Size = new System.Drawing.Size(1561, 286);
            this.tblExperienciaLaboral.TabIndex = 0;
            // 
            // tabIdiomas
            // 
            this.tabIdiomas.Controls.Add(this.tblIdiomas);
            this.tabIdiomas.Location = new System.Drawing.Point(10, 48);
            this.tabIdiomas.Name = "tabIdiomas";
            this.tabIdiomas.Size = new System.Drawing.Size(1613, 356);
            this.tabIdiomas.TabIndex = 3;
            this.tabIdiomas.Text = "Idiomas";
            this.tabIdiomas.UseVisualStyleBackColor = true;
            // 
            // tblIdiomas
            // 
            this.tblIdiomas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblIdiomas.Location = new System.Drawing.Point(31, 45);
            this.tblIdiomas.Name = "tblIdiomas";
            this.tblIdiomas.RowTemplate.Height = 40;
            this.tblIdiomas.Size = new System.Drawing.Size(1562, 286);
            this.tblIdiomas.TabIndex = 0;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(1430, 1199);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(397, 61);
            this.btnGuardar.TabIndex = 0;
            this.btnGuardar.Text = "Enviar Solicitud";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblNombrePuestoAspira
            // 
            this.lblNombrePuestoAspira.AutoSize = true;
            this.lblNombrePuestoAspira.Font = new System.Drawing.Font("jdFontAwesome", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrePuestoAspira.Location = new System.Drawing.Point(53, 68);
            this.lblNombrePuestoAspira.Name = "lblNombrePuestoAspira";
            this.lblNombrePuestoAspira.Size = new System.Drawing.Size(853, 54);
            this.lblNombrePuestoAspira.TabIndex = 8;
            this.lblNombrePuestoAspira.Text = "Ingresar Información para aplicar al puesto: ";
            // 
            // btnRechazarSolicitud
            // 
            this.btnRechazarSolicitud.Location = new System.Drawing.Point(39, 1214);
            this.btnRechazarSolicitud.Name = "btnRechazarSolicitud";
            this.btnRechazarSolicitud.Size = new System.Drawing.Size(397, 61);
            this.btnRechazarSolicitud.TabIndex = 14;
            this.btnRechazarSolicitud.Text = "Rechazar Solicitud";
            this.btnRechazarSolicitud.UseVisualStyleBackColor = true;
            this.btnRechazarSolicitud.Visible = false;
            this.btnRechazarSolicitud.Click += new System.EventHandler(this.btnRechazarSolicitud_Click);
            // 
            // lblEstadoSolicitud
            // 
            this.lblEstadoSolicitud.AutoSize = true;
            this.lblEstadoSolicitud.Font = new System.Drawing.Font("jdFontAwesome", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoSolicitud.Location = new System.Drawing.Point(1022, 68);
            this.lblEstadoSolicitud.Name = "lblEstadoSolicitud";
            this.lblEstadoSolicitud.Size = new System.Drawing.Size(438, 54);
            this.lblEstadoSolicitud.TabIndex = 15;
            this.lblEstadoSolicitud.Text = "Estado de la solicitud:";
            this.lblEstadoSolicitud.Visible = false;
            // 
            // btnAceptarSolicitud
            // 
            this.btnAceptarSolicitud.Location = new System.Drawing.Point(498, 1214);
            this.btnAceptarSolicitud.Name = "btnAceptarSolicitud";
            this.btnAceptarSolicitud.Size = new System.Drawing.Size(397, 61);
            this.btnAceptarSolicitud.TabIndex = 16;
            this.btnAceptarSolicitud.Text = "Aceptar Solicitud";
            this.btnAceptarSolicitud.UseVisualStyleBackColor = true;
            this.btnAceptarSolicitud.Visible = false;
            this.btnAceptarSolicitud.Click += new System.EventHandler(this.btnAceptarSolicitud_Click);
            // 
            // CandidateView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1897, 1287);
            this.Controls.Add(this.btnAceptarSolicitud);
            this.Controls.Add(this.lblEstadoSolicitud);
            this.Controls.Add(this.btnRechazarSolicitud);
            this.Controls.Add(this.lblNombrePuestoAspira);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CandidateView";
            this.Text = "CandidateView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabCompetencias.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblCompetencias)).EndInit();
            this.tabCapacitaciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblCapacitaciones)).EndInit();
            this.tabExperienciaLaboral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblExperienciaLaboral)).EndInit();
            this.tabIdiomas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblIdiomas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTipoIdentificacion;
        private System.Windows.Forms.TextBox txtNumeroIdentifacion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbDepartamento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSalarioAspirado;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnEliminarInformacionComplementaria;
        private System.Windows.Forms.Button btnAgregarInformacionComplementaria;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCompetencias;
        private System.Windows.Forms.DataGridView tblCompetencias;
        private System.Windows.Forms.TabPage tabCapacitaciones;
        private System.Windows.Forms.DataGridView tblCapacitaciones;
        private System.Windows.Forms.TabPage tabExperienciaLaboral;
        private System.Windows.Forms.DataGridView tblExperienciaLaboral;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label lblNombrePuestoAspira;
        private System.Windows.Forms.TextBox txtRecomendadoPor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabIdiomas;
        private System.Windows.Forms.DataGridView tblIdiomas;
        private System.Windows.Forms.Button btnRechazarSolicitud;
        private System.Windows.Forms.Label lblEstadoSolicitud;
        private System.Windows.Forms.Button btnAceptarSolicitud;
    }
}