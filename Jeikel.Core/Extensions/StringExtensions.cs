﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ObtenerDescriptionDesdeEnum<T>(this string name)
        {
            //get the member info of the enum
            MemberInfo[] memberInfos = typeof(T).GetMembers();
            if (memberInfos.Length > 0)
            {
                //loop through the member info classes
                foreach (var attributes in from memberInfo in memberInfos
                                           let attributes =
                                           memberInfo.GetCustomAttributes(typeof(DescriptionAttribute), false)
                                           where attributes.Length > 0
                                           where memberInfo.Name == name
                                           select attributes)
                {
                    var descriptionAttribute = (DescriptionAttribute)attributes.FirstOrDefault();
                    if (descriptionAttribute != null)
                        return descriptionAttribute.Description;
                }
            }

            //this means the enum was not found from the description, so return the default
            return String.Empty;
        }




        public static string ObtenerDescriptionDeEnum<T>(this T name) where T : struct
        {
            //get the member info of the enum
            MemberInfo[] memberInfos = typeof(T).GetMembers();
            if (memberInfos.Length > 0)
            {
                //loop through the member info classes
                foreach (var attributes in from memberInfo in memberInfos
                                           let attributes = memberInfo.GetCustomAttributes(typeof(DescriptionAttribute), false)
                                           where attributes.Length > 0
                                           select attributes)
                {
                    var descriptionAttribute = (DescriptionAttribute)attributes.FirstOrDefault();
                    if (descriptionAttribute != null)
                        return descriptionAttribute.Description;
                    return Enum.GetName(typeof(T), name);
                }
            }

            //this means the enum was not found from the description, so return the default
            return String.Empty;
        }


    }
}
