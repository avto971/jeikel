﻿using Jeikel.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Extensions
{
    public static class EnumExtensions
    {
        public static T ToInt32<T>(this int s) where T : struct
        {
            return (T)Enum.Parse(typeof(T), s.ToString(), true);
        }

        public static IEnumerable<T> ToArray<T>(this int[] s) where T : struct
        {
            for (int i = 0; i < s.Length; i++)
                yield return (T)Enum.Parse(typeof(T), s[i].ToString(), true);
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action(item);
            }
        }

        public static string GetDescription(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            DescriptionAttribute attribute
            = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
            as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }

        public static T GetEnum<T>(this string value)
        {
            //var names = Enum.GetNames(typeof(T));
            return (T)Enum.Parse(typeof(T), value, true);
        }


        public static IEnumerable<SelectItemOption> ToListOptions<TEnum>(this TEnum enumeracion)
        {
            var enumType = typeof(TEnum);
            var values = Enum.GetNames(enumType)
            .Select(enumerator => new
            {
                Descripcion = enumerator.ObtenerDescriptionDesdeEnum<TEnum>(),
                Real = enumerator
            }).ToList();

            var items = new List<SelectItemOption>();
            for (int i = 0; i < values.Count; i++)
            {
                items.Add(new SelectItemOption
                {
                    Key = values[i].Descripcion.ToString(CultureInfo.InvariantCulture),
                    Value = (int)Enum.Parse(typeof(TEnum), values[i].Real)
                });
            }


            return items;
        }
        public static IEnumerable<string> ToListOptionsText<TEnum>(this TEnum enumeracion)
        {
            var enumType = typeof(TEnum);
            var values = Enum.GetNames(enumType)
            .Select(enumerator => new
            {
                Descripcion = enumerator.ObtenerDescriptionDesdeEnum<TEnum>(),
                Real = enumerator
            }).ToList();

            var items = new List<string>();
            for (int i = 0; i < values.Count; i++)
            {
                items.Add(values[i].Descripcion.ToString(CultureInfo.InvariantCulture));

            }


            return items;
        }


    }
}
