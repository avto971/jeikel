﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Generic
{
    public interface IUserBase
    {
         string FirstName { get; set; }
         string LastName { get; set; }
    }
}
