﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Generic
{
    public interface IGenericRepository<T>
    {
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] include);
        void Add(T Entity);
        void Delete(T Entity);
        T FindById(int id);
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        IEnumerable<T> FindById(int id, params Expression<Func<T, object>>[] include);
        void Update(T Entity);
        void SaveChanges();
        EntityActionResult<T> ValidateEntity<T2>(T EntityToValidate, Action<T> callBack) where T2 : AbstractValidator<T>, new();
        IEnumerable<T> GetAll();

    }
}
