﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Generic
{
    public class FormContstants
    {
        public const int LOGINVIEWWIDTH = 300;
        public const int LOGINVIEWHEIGHT = 380;
        public const int ADMINMAINTENANCEHEIGHT = 500;
        public const int ADMINMAINTENANCEWIDTH = 720;
    }
}
