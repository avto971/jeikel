﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Generic
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        bool Deleted { get; set; }
        int CreatedBy { get; set; }
        int UpdatedBy { get; set; }
    }
}
