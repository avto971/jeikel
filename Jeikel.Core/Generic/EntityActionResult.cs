﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Generic
{
    public class EntityActionResult<T>
    {
        public string Message { get; set; }
        public int ErrorCode { get; set; }
        public bool Success { get; set; }
        public T Entity { get; set; }
    }
}
