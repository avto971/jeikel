﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Enums
{
    public enum RiskLevel
    {
        [Description("Alto Riesgo")]
        Alto,
        [Description("Medio Riesgo")]
        Medio,
        [Description("Bajo Riesgo")]
        Bajo
    }
}
