﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Enums
{
    public enum CandidateRequestStatus
    {
        
        [Description("En espera")]
        Espera,
        [Description("Rechazado")]
        Rechazada,
        [Description("Aprobada")]
        Aprobada
    }
}
