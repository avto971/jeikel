﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.Core.Enums
{
    public enum CapacitationLevel
    {
        [Description("Grado")]
        Grado,
        [Description("Post Grado")]
        PostGrado,
        [Description("Maestría")]
        Maestria,
        [Description("Técnico")]
        Tecnico,
    }
}
