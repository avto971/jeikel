﻿using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Context
{
    public class JeikelContext: DbContext
    {
        public JeikelContext() : base("name=JeikelDBConnectionString")
        {
            
        }


        public override int SaveChanges()
        {
            RunAudit();
            return base.SaveChanges();
        }
        private void RunAudit()
        {
            CreateAuditLogInformation();
        }

        public void CreateAuditLogInformation()
        {
            foreach (var entry in ChangeTracker.Entries<IBaseEntity>().Where(x => x.State == EntityState.Deleted))
            {
                var entity = entry.Entity;
                entity.Deleted = true;
            }
        }

        public DbSet<CandidateCapacitation> CandidateCapacitations { get; set; }
        public DbSet<CandidateWorkExperience> CandidateWorkExperiences { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<CandidateCompetence> CandidateCompetences { get; set; }
        public DbSet<CandidateLanguage> CandidateLanguages { get; set; }
        //public DbSet<Capacitation> Capacitations { get; set; }
        public DbSet<Competences> Competitions { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<IdentificationType> IdentificationTypes { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<JobPosition> JobPositions { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Rol> Rols { get; set; }
    }
}
