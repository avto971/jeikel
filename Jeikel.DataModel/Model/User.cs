﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
   public class User: IUserBase
    {
        public int Id { get; set; }
        public string FirstName { get ; set ; }
        public string LastName { get ; set ; }
        public int RolId { get; set; }
        public string UserName { get; set; }
        // the mapped-to-column property 
        public string Password { get; set; }
        public virtual Rol Rol { get; set; }
    }





}
