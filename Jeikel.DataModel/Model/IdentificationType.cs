﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class IdentificationType : IBaseEntity
    {
        public int Id { get ; set ; }
        public string Name { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }
    }
}
