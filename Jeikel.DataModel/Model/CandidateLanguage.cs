﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class CandidateLanguage : IBaseEntity
    {
        public int Id { get ; set ; }

        public int LanguageId { get; set; }
        public int CandidateId { get; set; }

        [NotMapped]
        public string LanguageName { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }

        public virtual Candidate Candidate { get; set; }

        public virtual Language Language { get; set; }
    }
}
