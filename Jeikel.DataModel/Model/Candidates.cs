﻿using Jeikel.Core.Enums;
using Jeikel.Core.Generic;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jeikel.DataModel.Model
{
    public class Candidate : IBaseEntity
    {
        public Candidate()
        {
            CandidatesCompetences = new List<CandidateCompetence>();
            CandidateCapacitations = new List<CandidateCapacitation>();
            CandidateWorkExperiences = new List<CandidateWorkExperience>();
            CandidateLanguages = new List<CandidateLanguage>();

            
        }
        public int Id { get ; set ; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdentificationTypeId { get; set; }
        public string Identification { get; set; }

        public int JobPositionId { get; set; }
        public int DepartmentId { get; set; }

        public decimal SalaryDesired { get; set; }

        public string RecommendedBy { get; set; }

        public CandidateRequestStatus CandidateRequestStatus { get; set; }

        [NotMapped]
        public string IdentificationTypeName { get { return IdentificationType?.Name; } set { this.idenName = value; }  }

        public string idenName;
        [NotMapped]
        public string DepartmentName { get { return Department?.Name; } }

        [NotMapped]
        public string JobPositionName { get { return this.JobPosition?.Name; } }

        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }

        public virtual List<CandidateCompetence> CandidatesCompetences { get; set; }
        public virtual List<CandidateCapacitation> CandidateCapacitations { get; set; }
        public virtual List<CandidateWorkExperience> CandidateWorkExperiences { get; set; }
        public virtual List<CandidateLanguage> CandidateLanguages { get; set; }
        public virtual  Department Department { get; set; }
        public virtual  JobPosition JobPosition { get; set; }
        public virtual IdentificationType IdentificationType { get; set; }
    }
}
