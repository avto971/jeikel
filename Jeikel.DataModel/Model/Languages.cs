﻿using Jeikel.Core.Generic;
using System;

namespace Jeikel.DataModel.Model
{
    public class Language : IBaseEntity
    {
        public int Id { get ; set ; }
        public bool Deleted { get ; set ; }
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }
    }
}
