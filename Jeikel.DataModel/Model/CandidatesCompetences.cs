﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class CandidateCompetence : IBaseEntity
    {
        public int Id { get ; set ; }
        public int CandidateId { get; set; }
        public int CompetitionId { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }

        [NotMapped]
        public string CompetitionName { get; set; }
   

        public virtual Candidate Candidate { get; set; }
        public virtual Competences Competition { get; set; }
    }
}
