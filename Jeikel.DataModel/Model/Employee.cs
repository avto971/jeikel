﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class Employee : IBaseEntity
    {
        public int Id { get ; set ; }
        public int CandidateId { get; set; }
        public DateTime DateOfAdmission { get; set; }
        public decimal Salary { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }

        public virtual Candidate Candidate { get; set; }
    }
}
