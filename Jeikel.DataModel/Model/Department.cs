﻿using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class Department: IBaseEntity
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public int Id { get ; set ; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }
    }
}
