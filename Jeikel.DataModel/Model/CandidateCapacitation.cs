﻿using Jeikel.Core.Enums;
using Jeikel.Core.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.DataModel.Model
{
    public class CandidateCapacitation : IBaseEntity
    {
        public int Id { get ; set ; }
        public int CandidateId { get; set; }
        public int IntitutionId { get; set; }

        public string Description { get; set; }
        public CapacitationLevel Level { get; set; }
        public DateTime From { get; set; }
        public DateTime Until { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }

        [NotMapped]
        public string InstitutionName { get; set; }
        [NotMapped]
        public string CapacitationLevelName { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual Institution Intitution { get; set; }
    }
}
