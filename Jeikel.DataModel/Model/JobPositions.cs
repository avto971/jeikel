﻿using Jeikel.Core.Enums;
using Jeikel.Core.Generic;

namespace Jeikel.DataModel.Model
{
    public class JobPosition : IBaseEntity
    {
        public int Id { get ; set ; }
        public string Name { get; set; }
        public RiskLevel RiskLevel { get; set; }
        public decimal  MinimunSalary { get; set; }
        public decimal  MaximumSalary { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }
    }
}
