﻿using Jeikel.Core.Generic;

namespace Jeikel.DataModel.Model
{
    public class Competences : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Deleted { get ; set ; }
        public int CreatedBy { get ; set ; }
        public int UpdatedBy { get ; set ; }
    }
}
