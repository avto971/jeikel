namespace Jeikel.DataModel.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Jeikel.DataModel.Context.JeikelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Jeikel.DataModel.Context.JeikelContext context)
        {
            context
                .Rols.AddOrUpdate(new Model.Rol { RolName = "Admin", RolCode = "001" });

            context.Users.AddOrUpdate(new Model.User
            {
                UserName = "Admin",
                FirstName = "Angel",
                LastName = "Torres",
                RolId = context.Rols.FirstAsync().Id,
                Password = "Code1234,"
            });

            context.IdentificationTypes.AddOrUpdate(new Model.IdentificationType
            {
                Name = "C�dula"
            },new Model.IdentificationType { Name ="Pasaporte" });
        }
    }
}
