﻿using Jeikel.BL.Repositories.Base;
using Jeikel.BL.Services.Base;
using Jeikel.BL.Services.Implementations;
using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.Core.Generic;
using SimpleInjector;

namespace Jeikel.BL.DL
{
    
    public static class Registry
    {
        static Container container;

        public static Container registerContainer()
        {
           container = new Container();

            // 2. Configure the container (register)
            container.Register<ITestInterfaceService, TestInterfaceService>();
            container.Register(typeof(IGenericRepository<>),typeof(GenericRepository<>));
            container.Register(typeof(IGenericService<>),typeof(GenericService<>));
            #region Services
                container.Register<ILanguageService, LanguageService>();
                container.Register<IInstitutionService, InstitutionService>();
                container.Register<IIdentificationTypeService, IdentificationTypeService>();
                container.Register<IDepartmentService, DepartmentService>();
                container.Register<ICompetitionService, CompetitionService>();
                container.Register<IJobPositionService, JobPositionService>();
                container.Register<ICandidateService, CandidateService>();
                container.Register<IEmployeeService, EmployeeService>();
            #endregion

            // 3. Verify your configuration
            container.Verify();

            return container;
        }
    }
}
