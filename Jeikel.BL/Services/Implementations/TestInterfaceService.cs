﻿using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Validators;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Services.Implementations
{
    public class TestInterfaceService : ITestInterfaceService
    {
        private IGenericRepository<Language> _repo { get; set; }
        public TestInterfaceService(IGenericRepository<Language> repo)
        {
            _repo = repo;
        }
        public void AddTest()
        {
           var len =  _repo.ValidateEntity<LanguageValidator>(new Language { LanguageCode ="klk", LanguageName="klk" },(entity)=> {
               _repo.Add(entity);
               _repo.SaveChanges();
            });
        }

        public string ReturnHelloWork()
        {
            return "Hello World";
        }
    }
}
