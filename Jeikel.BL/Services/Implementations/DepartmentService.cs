﻿using Jeikel.BL.Services.Base;
using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Services.Implementations
{
    public class DepartmentService : GenericService<Department>, IDepartmentService
    {
        public DepartmentService(IGenericRepository<Department> repository) : base(repository)
        { }
    }
}
