﻿using Jeikel.BL.Services.Base;
using Jeikel.BL.Services.Interfaces;
using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;

namespace Jeikel.BL.Services.Implementations
{
    public class CompetitionService : GenericService<Competences>, IGenericService<Competences>, ICompetitionService
    {
        public CompetitionService(IGenericRepository<Competences> repository) : base(repository)
        { }
    }
}
