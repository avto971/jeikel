﻿using Jeikel.BL.Services.Base;
using Jeikel.BL.Services.Interfaces;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Services.Implementations
{
    public class InstitutionService : GenericService<Institution>, IInstitutionService
    {
        public InstitutionService(IGenericRepository<Institution> repository) : base(repository)
        {

        }
    }
}
