﻿using Jeikel.Core.Enums;
using Jeikel.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Services
{



    public static  class EnumService
    {
        public static  IEnumerable<SelectItemOption> GetRiskLevel()
        {
            List<SelectItemOption> items = new RiskLevel().ToListOptions().ToList();
            return items;
        }

        public static IEnumerable<SelectItemOption> GetCapacitationLevel()
        {
            List<SelectItemOption> items = new CapacitationLevel().ToListOptions().ToList();
            return items;
        }
    }
}
