﻿using FluentValidation;
using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Services.Base   
{
    public class GenericService<T> : IGenericService<T>
    {
        public IGenericRepository<T> Repository { get { return this._genericRepository; } }
        private readonly IGenericRepository<T> _genericRepository;
        public GenericService(IGenericRepository<T> repository)
        {
            this._genericRepository = repository;
        }

        public EntityActionResult<T> SaveOrEdit<T2>(T entity, bool isEditing) where T2 : AbstractValidator<T>, new()
        {

            var actionResult = this.Repository.ValidateEntity<T2>(entity,
                     (language) => {

                         if (isEditing)
                         {
                             Repository.Update(language);
                         }
                         else
                         {
                             Repository.Add(language);
                         }

                         Repository.SaveChanges();
                     });
            
            return actionResult;

        }

        public void DeleteEntity(T Entity, Action callBack)
        {
            Repository.Delete(Entity);
            Repository.SaveChanges();
            callBack();
        }
    }
}
