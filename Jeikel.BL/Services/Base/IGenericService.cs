﻿using FluentValidation;
using Jeikel.Core.Generic;
using System;

namespace Jeikel.BL.Services.Interfaces.Base
{
    public interface IGenericService<T>
    {
        IGenericRepository<T> Repository { get; }

        EntityActionResult<T> SaveOrEdit<T2>(T entity, bool isEditing) where T2 : AbstractValidator<T>, new();

        void DeleteEntity(T Entity, Action callBack);
    }
}