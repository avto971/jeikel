﻿using Jeikel.BL.Services.Interfaces.Base;
using Jeikel.DataModel.Model;

namespace Jeikel.BL.Services.Interfaces
{
    public interface IJobPositionService: IGenericService<JobPosition>
    {
    }
}
