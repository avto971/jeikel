﻿using FluentValidation;
using Jeikel.BL.Services.Interfaces;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jeikel.BL.Validators
{
    public class CandidateValidator : AbstractValidator<Candidate>
    {
        public CandidateValidator()
        {

            RuleFor(x => x.IdentificationTypeId).NotEqual(0);
            RuleFor(x => x.Identification).Must(validaCedula)
                .When(AssertIsCedula).WithMessage("Cédula Inválida");
            RuleFor(x => x.SalaryDesired)
                .GreaterThan(0).WithMessage("El valor del salario deseado debe ser mayor a 0");
            RuleFor(x => x.CandidateCapacitations)
                .Must(CandidateCapacitationsValidation)
                .When((candidate) => { return candidate.CandidateCapacitations.Count > 0; })
                .WithMessage("Verifique las fechas de las capacitaciones," +
                " la fecha inicio no puede ser mayor");
            RuleFor(x => x.CandidateWorkExperiences)
                .Must(CandidateWorkExperienceValidation)
                .When((candidate) => { return candidate.CandidateWorkExperiences.Count > 0; })
                .WithMessage("Verifique las fechas de las experiencias laborales, " +
                "la fecha inicio no puede ser mayor");

            RuleFor(x => x.FirstName).NotNull().WithMessage("Debe escribir un nombre");
            RuleFor(x => x.LastName).NotNull().WithMessage("Debe escribir un apellido");
            RuleFor(x => x.RecommendedBy).NotNull().WithMessage("Debe escribir quién lo recomendó");
            RuleFor(x => x.DepartmentId).NotEqual(0).WithMessage("Debe seleccionar un departamento");
        }

        public bool AssertIsCedula(Candidate candidate)
        {
            return candidate.idenName.ToLower() == "cédula";
        }
        public bool CandidateCapacitationsValidation(List<CandidateCapacitation> capacitations)
        {
            foreach (var candidate in capacitations)
            {
                if (candidate.From > candidate.Until)
                    return false;
            }
            return true;
        }

        public bool CandidateWorkExperienceValidation(List<CandidateWorkExperience> workExperince)
        {
            foreach (var WExperience in workExperince)
            {
                if (WExperience.From > WExperience.Until)
                    return false;
            }
            return true;
        }



        public static bool validaCedula(string pCedula)
        {
            int vnTotal = 0;
            string vcCedula = pCedula.Replace("-", "");
            int pLongCed = vcCedula.Trim().Length;
            int[] digitoMult = new int[11] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1 };
            if (pLongCed < 11 || pLongCed > 11)
                return false;
            for (int vDig = 1; vDig <= pLongCed; vDig++)
            {
                int vCalculo = Int32.Parse(vcCedula.Substring(vDig - 1, 1)) * digitoMult[vDig - 1];
                if (vCalculo < 10)
                    vnTotal += vCalculo;
                else
                    vnTotal += Int32.Parse(vCalculo.ToString().Substring(0, 1)) + Int32.Parse(vCalculo.ToString().Substring(1, 1));
            }
            if (vnTotal % 10 == 0)
                return true;
            else
                return false;

        }
    }
}
