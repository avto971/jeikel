﻿using FluentValidation;
using Jeikel.DataModel.Model;

namespace Jeikel.BL.Validators
{
    public class EmployeeValidator: AbstractValidator<Employee>
    {
        public EmployeeValidator()
        {
            RuleFor(x => x.Salary).GreaterThan(0);
        }
    }
}
