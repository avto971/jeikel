﻿using FluentValidation;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Validators
{
    public class CompetitionValidator : AbstractValidator<Competences>
    {
        public CompetitionValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().WithMessage("Tiene que especificar un nombre a la competencia");
            RuleFor(x => x.Description).NotNull().NotEmpty().WithMessage("Tiene que especificar una descripción");

        }
    }
}
