﻿using FluentValidation;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Validators
{
    public class DepartmentValidator: AbstractValidator<Department>
    {
        public DepartmentValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().WithMessage("Tiene que especificar un nombre al departamento");
            RuleFor(x => x.Description).NotNull().NotEmpty().WithMessage("Tiene que especificar una descripción");
        }
    }
}
