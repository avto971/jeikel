﻿using FluentValidation;
using Jeikel.DataModel.Model;

namespace Jeikel.BL.Validators
{
    public class CandidateCapacitationValidator: AbstractValidator<CandidateCapacitation>
    {
        public CandidateCapacitationValidator()
        {
            RuleFor(x => x.IntitutionId).NotEqual(0).WithMessage("Error, debe seleccionar una institución");
            RuleFor(x => x.Description).NotNull().NotEmpty().WithMessage("Error, debe escribir una descripción");
        }
    }
}
