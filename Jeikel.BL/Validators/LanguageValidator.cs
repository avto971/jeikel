﻿using FluentValidation;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Validators
{
    public class LanguageValidator: AbstractValidator<Language>
    {
        public LanguageValidator()
        {
            RuleFor(x => x.LanguageName).NotNull().NotEmpty().WithMessage("Tiene que especificar un nombre para el lenguaje");
            RuleFor(x => x.LanguageCode).NotNull().NotEmpty().WithMessage("Tiene que especificar un código para el lenguaje");
        }
    }
}
