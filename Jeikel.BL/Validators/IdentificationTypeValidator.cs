﻿using FluentValidation;
using Jeikel.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Validators
{
    public class IdentificationTypeValidator: AbstractValidator<IdentificationType>
    {
        public IdentificationTypeValidator()
        {
            RuleFor(x=>x.Name).NotNull().NotEmpty().WithMessage("Tiene que especificar un nombre para la identifiación");
        }
    }
}
