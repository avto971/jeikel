﻿using FluentValidation;
using Jeikel.DataModel.Model;

namespace Jeikel.BL.Validators
{
    public class JobPositionValidator: AbstractValidator<JobPosition>
    {
        public JobPositionValidator()
        {
            RuleFor(x=>x.Name).NotNull().NotEmpty().WithMessage("Tiene que especificar un nombre la institución");
            RuleFor(x => x).Must((jobPos)=> { return jobPos.MaximumSalary > jobPos.MinimunSalary; }).WithMessage("El salario máximo no puede ser menor que el salario mínimo");
        }
    }
}
