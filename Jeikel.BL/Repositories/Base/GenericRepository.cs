﻿using FluentValidation;
using FluentValidation.Results;
using Jeikel.Core.Generic;
using Jeikel.DataModel.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Jeikel.BL.Repositories.Base
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, IBaseEntity, new()
    {
        protected DbContext _context;
        public DbSet<T> _set;
        public GenericRepository()
        {
            _context = new JeikelContext();
            _set = _context.Set<T>();
        }
        public void Add(T Entity)
        {
            _set.Add(Entity);

        }

        public void Delete(T Entity)
        {
            _set.Remove(Entity);

        }

        public T FindById(int id)
        {
            var entity = _set.FirstOrDefault(x => x.Id == id && !x.Deleted);
            return entity;
        }

        public IEnumerable<T> FindById(int id, params Expression<Func<T, object>>[] include)
        {
            var query = _set.AsQueryable();

            foreach (var item in include)
            {
                query = query.Include(item);
            }

            var entity = query.Where(x => x.Id == id && !x.Deleted);
            return entity;
        }

        public IEnumerable<T> GetAll(params Expression<Func<T, object>>[] include)
        {
            var query = _set.AsQueryable();

            foreach (var item in include)
            {
                query = query.Include(item);
            }

            return query.ToList();
        }

        public IEnumerable<T> GetAll()
        {
            return _set.ToList();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(T Entity)
        {
            _context.Entry(Entity).State = EntityState.Modified;
        }


        public EntityActionResult<T> ValidateEntity<T2>(T EntityToValidate, Action<T> callBack) where T2 : AbstractValidator<T>, new()
        {
            T Entity = new T();
            T2 validator = new T2();
            ValidationResult results = validator.Validate(EntityToValidate);

            bool validationSucceeded = results.IsValid;
            IList<ValidationFailure> failures = results.Errors;
            var messages = string.Join(",", failures.Select(x => x.ErrorMessage));

            if (results.IsValid)
            {
                callBack(EntityToValidate);
            }
            return new EntityActionResult<T>
            {
                Success = results.IsValid,
                Message = results.IsValid ? "Validado con exito" : messages,
                ErrorCode = 500,
                Entity = EntityToValidate
            };

        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            var query = _set.AsQueryable();
            return query.Where(expression).AsEnumerable();
        }
    }
}
